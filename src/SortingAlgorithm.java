//Name: Mr. Jesse Byers
//Student Number: 11559304
//Instructor: Dr Md Anisur Rahman, PhD
//Date: 06/10/2019
//Class Name: SortingAlgorithm.java

//Description: Creates an array of integers and displays them in a bar chart, then sorts the array using
//bubble sort, insertion sort and selection sort showing the process.


import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Creates a sorting algorithm animation and runs
 */
public class SortingAlgorithm extends Application {
    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) throws InterruptedException {

        //SET UP ARRAY
        Integer[] numberArray = new Integer[31];

        Long threadSleep = 800L;

        //initialise array with numbers
        for (int i = 0; i < numberArray.length; i++) {

            numberArray[i] = i;
        }

        //shuffle array
        List<Integer> l = Arrays.asList(numberArray);
        Collections.shuffle(l);

        //put shuffled list back into array
        for (int i = 0; i < l.size(); i++) {

            numberArray[0] = l.get(0);
        }


        //PANE ORGANISATION
        GridPane gridpane = new GridPane();

        BarChart bubbleChart = setBubbleBarChart(numberArray);
        gridpane.add(new Label("Bubble Sort"), 0, 0);
        gridpane.add(bubbleChart, 0, 1);

        gridpane.add(new Label("Insertion Sort"), 0, 2);
        BarChart insertionChart = setInsertionBarChart(numberArray);
        gridpane.add(insertionChart, 0, 3);

        gridpane.add(new Label("Selection Sort"), 0, 4);
        BarChart selectionChart = setSelectionBarChart(numberArray);
        gridpane.add(selectionChart, 0, 5);

        //Set Stage
        Scene scene = new Scene(gridpane, 250, 500);
        primaryStage.setTitle("Sorting Algorithm"); // Set the stage title
        primaryStage.setScene(scene); // Place the scene in the stage
        primaryStage.show(); // Display the stage

        //bubble sort thread
        new Thread(new Runnable() {
            int i, j, temp, x;


            @Override
            public void run() {
                //bubble sort algorithm
                for (i = numberArray.length - 1; i > 0; i--) {
                    for (j = 0; j < i; j++) {
                        if (numberArray[j] > numberArray[j + 1]) {
                            temp = numberArray[j];
                            numberArray[j] = numberArray[j + 1];
                            numberArray[j + 1] = temp;
                        }
                    }


                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {

                            //create chart
                            XYChart.Series dataSeries1 = new XYChart.Series();
                            dataSeries1.setName("Bubble Sort");
                            x = 0;
                            //add numbers to bar chart
                            for (int i = 0; i < numberArray.length; i++) {

                                dataSeries1.getData().add(new XYChart.Data(String.valueOf(x), numberArray[i]));
                                x++;

                            }

                            bubbleChart.getData().clear();
                            bubbleChart.getData().add(dataSeries1);

                        }
                    });
                    //put thread to sleep
                    try {
                        Thread.sleep(threadSleep);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }).start();

        //selection sort thread
        new Thread(new Runnable() {
            int i, j, temp, pos_greatest, x;

            @Override
            public void run() {
                //selection sort algorithm
                for (i = numberArray.length - 1; i > 0; i--) {
                    pos_greatest = 0;
                    for (j = 0; j <= i; j++) {
                        if (numberArray[j] > numberArray[pos_greatest])
                            pos_greatest = j;

                        temp = numberArray[i];
                        numberArray[i] = numberArray[pos_greatest];
                        numberArray[pos_greatest] = temp;
                    }


                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {

                            //create chart and display
                            XYChart.Series dataSeries2 = new XYChart.Series();
                            dataSeries2.setName("Selection Sort");
                            x = 0;
                            //add numbers to bar chart
                            for (int i = 0; i < numberArray.length; i++) {

                                dataSeries2.getData().add(new XYChart.Data(String.valueOf(x), numberArray[i]));
                                x++;
                            }

                            selectionChart.getData().clear();
                            selectionChart.getData().add(dataSeries2);

                        }

                    });
                    //put thread to sleep
                    try {
                        Thread.sleep(threadSleep);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }).start();


        //insertion sort thread
        new Thread(new Runnable() {
            int i, j, temp, x;

            @Override
            public void run() {
                //insertion sort algorithm
                for (j = 1; j < numberArray.length - 1; j++) {

                    temp = numberArray[j];
                    i = j; // range 0 to j-1 is sorted

                    while (i > 0 && numberArray[i - 1] >= temp) {
                        numberArray[i] = numberArray[i - 1];
                        i--;
                    }
                    numberArray[i] = temp;

                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            //create chart
                            XYChart.Series dataSeries3 = new XYChart.Series();
                            dataSeries3.setName("Insertion Sort");
                            x = 0;
                            //add numbers to bar chart
                            for (int i = 0; i < numberArray.length; i++) {

                                dataSeries3.getData().add(new XYChart.Data(String.valueOf(x), numberArray[i]));
                                x++;
                            }

                            insertionChart.getData().clear();
                            insertionChart.getData().add(dataSeries3);

                        }
                    });
                    //put thread to sleep
                    try {
                        Thread.sleep(threadSleep);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }).start();
    }

    /**
     * Main method
     *
     * @since 1.8
     */
    public static void main(String[] args) {
        Application.launch(args);
    }


    /**
     * Creates an initial bar chart for bubble sort
     *
     * @param numberArray shuffled array of integers 1 to 30
     * @since 1.8
     */
    public BarChart setBubbleBarChart(Integer[] numberArray) {

        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();

        BarChart bubbleSort = new BarChart<>(xAxis, yAxis);
        bubbleSort.setMaxWidth(50);
        bubbleSort.setMaxHeight(50);

        XYChart.Series dataSeries1 = new XYChart.Series();
        dataSeries1.setName("Bubble Sort");

        int x = 0;

        //add numbers to bar chart
        for (int i = 0; i < numberArray.length; i++) {

            dataSeries1.getData().add(new XYChart.Data(String.valueOf(x), numberArray[i]));
            x++;
        }

        bubbleSort.getData().add(dataSeries1);

        return bubbleSort;
    }

    /**
     * Creates an initial bar chart for insertion sort
     *
     * @param numberArray shuffled array of integers 1 to 30
     * @since 1.8
     */
    public BarChart setInsertionBarChart(Integer[] numberArray) {

        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();

        BarChart insertionSort = new BarChart<>(xAxis, yAxis);
        insertionSort.setMaxWidth(50);
        insertionSort.setMaxHeight(50);

        XYChart.Series dataSeries2 = new XYChart.Series();
        dataSeries2.setName("Insertion Sort");

        int x = 0;

        //add numbers to bar chart
        for (int i = 0; i < numberArray.length; i++) {

            dataSeries2.getData().add(new XYChart.Data(String.valueOf(x), numberArray[i]));
            x++;
        }

        insertionSort.getData().add(dataSeries2);

        return insertionSort;
    }

    /**
     * Creates an initial bar chart for selection sort
     *
     * @param numberArray shuffled array of integers 1 to 30
     * @since 1.8
     */
    public BarChart setSelectionBarChart(Integer[] numberArray) {

        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();

        BarChart selectionSort = new BarChart<>(xAxis, yAxis);
        selectionSort.setMaxWidth(50);
        selectionSort.setMaxHeight(50);

        XYChart.Series dataSeries3 = new XYChart.Series();
        dataSeries3.setName("Selection Sort");

        int x = 0;

        //add numbers to bar chart
        for (int i = 0; i < numberArray.length; i++) {

            dataSeries3.getData().add(new XYChart.Data(String.valueOf(x), numberArray[i]));
            x++;
        }

        selectionSort.getData().add(dataSeries3);

        return selectionSort;
    }

}
